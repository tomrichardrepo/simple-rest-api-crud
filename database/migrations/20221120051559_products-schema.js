/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {

    return knex.schema.createTable("products", tbl => {

        tbl.text("id", 50).unique().notNullable();
        tbl.text("product_name", 128).notNullable();
        tbl.text("product_photo", 255);
        tbl.integer("product_stock");
        tbl.decimal("product_price");

    });

};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {

    return knex.schema.dropTableIfExists('products');

};
