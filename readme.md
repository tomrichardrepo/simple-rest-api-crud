# Simple REST API CRUD

## About

Project ini dibuat untuk mempelajari konsep penggunaan  Rest API Insert, Update, Delete, dan Read.

### Install Packages

```
> npm install
```

### Migration

```
> npm install knex -g

> npm install npx

> npx knex migrate:latest

> npx knex seed:run
```

### Run Server

```javascript
// Hot Reloaded Server
> npm run server

// Prod Server
> npm run start
```

check server sudah berjalan di http://localhost:4000

### Documentation

import `simple-rest-api-crud.postman_collection.json` ke dalam postman untuk mendapatkan collection.

download postman disini : 

<a href="https://www.postman.com/downloads/">( download postman )</a>
