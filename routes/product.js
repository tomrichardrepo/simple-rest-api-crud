const express = require('express');
const fs = require('fs');
const { rootdir, updateRooturl } = require('../root');
const path = require('path');
const { checkRules } = require('../validators/checkRules');
const { body, param } = require('express-validator');
const router = express.Router();
const db = require("../database/models/product.model");
const isImage = require('is-image');
const multer = require('multer');
const upload = multer({ dest: 'storage/temp/' });

const idValidation = [
    param('id').matches(/[a-zA-Z0-9\-]{20,40}/)
]

const insertValidation = [
    body('id').matches(/[a-zA-Z0-9\-]{20,40}/).notEmpty(),
    body('product_name').isString().notEmpty(),
    body('product_stock').isNumeric().notEmpty(),
    body('product_price').isNumeric().notEmpty()
]

router.get("/v1.0/products/:id", async (req, res) => {
    try {
        const product = await db.findById(req.params.id).first();
        if (product) {
            res.status(200).json(updateRooturl(product, ['product_photo']))
        } else {
            res.status(204).json({ "message": "Empty Data" });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({
            error: "Server cannot find any products"
        });
    }
});

router.get("/v1.0/products", async (req, res) => {
    try {
        const products = await db.findAll();
        res.status(200).json(updateRooturl(products, ['product_photo']));
    } catch (err) {
        console.error(err);
        res.status(500).json({
            error: "Server cannot find any products"
        });
    }
});

router.get("/v1.0/products/:id/photo", (req, res) => {

    const pathFile = `${rootdir}/storage/products/`;

    const id = req.params.id;

    fs.readdir(pathFile, function (err, files) {
        if (err) {
            return res.status(404).json({});
        }
        const fileFiltered = files.filter((x) => x.match(new RegExp(`^${id}\.`)));

        if (fileFiltered) {
            res.status(200).sendFile(`${pathFile}${fileFiltered[0]}`);
        }

    });

});

router.post("/v1.0/products/new", insertValidation, checkRules, async (req, res) => {
    try {
        const products = await db.insert(req.body);
        products ? res.status(200).json({ "message": "insert success" }) : res.status(409).json({
            error: "Cannot insert the product."
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            error: "There was error when insert products into database."
        });
    }
});

router.post("/v1.0/products/:id/update", idValidation, checkRules, upload.single('product_photo'), async (req, res) => {

    let isUploadFile = false;
    let linkFile = "";

    try {
        if (req.file) {
            // validate and move file to destination
            const filePath = req.file.path;
            const originName = req.file.originalname;
            const fileName = req.file.filename;
            const fileExt = path.extname(originName);
            if (['.png', '.jpg', '.jpeg'].includes(fileExt)) {
                const fileDest = `${rootdir}/storage/products/${fileName}${fileExt}`;
                fs.renameSync(filePath, fileDest);
                isUploadFile = true;
                linkFile = `:root:/product-service/v1.0/products/${fileName}/photo`;
            } else {
                // remove the file
                fs.rmSync(filePath);
            }
        }
    } catch (e) {
        fs.rmSync(filePath);
    }

    try {

        if (isUploadFile) {
            req.body.product_photo = linkFile;
        }

        console.log(req.body);
        console.log(req.file);

        const products = await db.update(req.params.id, req.body);
        products ? res.status(200).json({ "message": "update success" }) : res.status(409).json({
            error: "Cannot update the product"
        })

    } catch (err) {
        console.error(err);
        res.status(500).json({
            error: "There was error when update products from database."
        });
    }
});

router.post("/v1.0/products/:id/delete", idValidation, checkRules, async (req, res) => {
    try {
        const products = await db.remove(req.params.id);
        products ? res.status(200).json({ "message": "delete success" }) : res.status(409).json({
            error: "Cannot delete the product."
        })
    } catch (err) {
        console.error(err);
        res.status(500).json({
            error: "There was error when delete products from database."
        });
    }
});

module.exports = router;