const express = require('express');
const { ValidationError } = require('express-validator');
const app = express();
const port = process.env.PORT | 4000;

/**
 * Services
 */
const productServices = require("./routes/product");

/** 
 * Middlewares
 */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


/** 
 * Routes 
 */
app.get('/', (req, res) => {
    res.send('Welcome to Web Crud Sample v1.0')
})

app.use('/product-service', productServices);

app.use(function (err, req, res, next) {
    if (err instanceof ValidationError) {
        return res.status(err.statusCode).json(err)
    }
    return res.status(500).json(err)
})

/**
 * Server Start
 */
app.listen(port, () => {
    console.log(`WCS listening on port ${port}`)
});