const { db } = require("../db.js");

module.exports = {
    findAll,
    findById,
    insert,
    update,
    remove
};

function findAll() {
    return db('products');
}

function findById(id) {
    return db('products').where({ id });
}

function insert(post) {
    return db('products').insert(post).then(ids => ({
        id: ids[0]
    }))
}

function update(id, post) {
    return db('products').where({ id }).update(post);
}

function remove(id) {
    return db('products').where({ id }).del();
}