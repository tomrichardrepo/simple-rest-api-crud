const rootdir = `${__dirname}/`;
const rooturl = `http://localhost:4000/`;

const updateRooturl = (obj, keys) => {

    const isObject = typeof obj === 'object' && !Array.isArray(obj);

    if (isObject) {
        obj = [obj];
    } else if (!Array.isArray(obj)) {
        return null;
    }

    obj = obj.map((val) => {
        keys.forEach(key => {
            val[key] = val[key].replace(/^\:root\:\//, rooturl);
        });
        return val;
    });

    if (isObject) {
        obj = obj[0];
    }

    return obj;

}

module.exports = {
    rootdir,
    rooturl,
    updateRooturl
}