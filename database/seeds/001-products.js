const { faker } = require('@faker-js/faker');

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

exports.seed = async function (knex) {

  const Products = [];

  Array.from({ length: 20 }).forEach(() => {
    Products.push({
      id: faker.datatype.uuid(),
      product_name: faker.commerce.productName(),
      product_photo: faker.image.food(),
      product_stock: faker.datatype.number(100),
      product_price: faker.commerce.price()
    })
  });

  // Deletes ALL existing entries
  await knex('products').del()
  await knex('products').insert(Products);
};
